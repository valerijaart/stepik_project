import unittest
from application import factorial

class TestFactorial(unittest.TestCase):
    def test_factorial_for_6(self):
        expected = 720
        result = factorial(6)
        self.assertEqual(expected, result)

    def test_factorial_for_10(self):
        expected = 3628800
        result = factorial(10)
        self.assertEqual(expected, result)

    def test_factorial_negative_value(self):
        expected = -1
        result = factorial(-10)
        self.assertEqual(expected, result)

    def test_factorial_incorrect_float_type(self):
        expected = -1
        result = factorial(15.4)
        self.assertEqual(expected, result)

    def test_factorial_incorrect_string_type(self):
        expected = -1
        result = factorial("10")
        self.assertEqual(expected, result)

    def test_factorial_incorrect_list_type(self):
        expected = -1
        result = factorial([10])
        self.assertEqual(expected, result)

if __name__ == '__main__':
    unittest.main()
