def factorial(n):
    if type(n) is not int or n < 0:
        return -1

    if n == 0 or n == 1:
        return 1
    return n * factorial(n-1)
